import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    data: []
}

export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        add: (state, action) => {
            state.data = [
                ...state.data,
                action.payload
            ]
        }
    },

});

// ini actions
export const { add } = todoSlice.actions;

// ini selector
export const selectData = (state) => state.todo.data;

export default todoSlice.reducer;