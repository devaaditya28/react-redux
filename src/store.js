import { configureStore } from '@reduxjs/toolkit';
import todoReducer from './reducers/todoSlicer';

export const store = configureStore({
    reducer: {
      todo: todoReducer
    },
});
