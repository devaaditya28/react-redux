import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  selectData,
  add
} from './reducers/todoSlicer';

function App() {
  const data = useSelector(selectData);
  const dispatch = useDispatch();
  const [form, setForm] = useState({})

  return (
    <div className="App" style={{ textAlign: 'center'}}> 
      <div>
        <label>Id</label>
        <br />
        <input type="text" name="id" onChange={(e) => setForm({
          ...form,
          id : e.target.value
        })}/>     
      </div>      
      <div>
        <label>Todo</label>
        <br />
        <input type="text" name="todo" onChange={(e) => setForm({
          ...form,
          todo : e.target.value
        })}/>     
      </div> 
      <button type="button" onClick={() => dispatch(add(form))}>Add</button>
      <br />
      <br />
      <br />
      <table>
        <tr>
          <td>No</td>
          <td>Todo</td>
        </tr>
        {data.map((v) => (
          <tr>
            <td>{v.id}</td>
            <td>{v.todo}</td>
          </tr>
        ))}
      </table>
    </div>
  );
}

export default App;